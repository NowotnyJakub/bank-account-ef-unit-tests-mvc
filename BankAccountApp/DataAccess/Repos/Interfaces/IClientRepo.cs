﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Repos.Interfaces
{
    public interface IClientRepo
    {
        void Create(Client client);
        IEnumerable<Client> GetAll();
        Client GetById(int id);
        void Update(Client client);
    }
}