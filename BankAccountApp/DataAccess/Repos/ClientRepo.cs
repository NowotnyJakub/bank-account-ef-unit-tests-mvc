﻿using System.Collections.Generic;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;

namespace DataAccess.Repos
{
    public class ClientRepo : IClientRepo
    {
        private BankDbContext _dbContext;
        public ClientRepo(BankDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Create(Client client)
        {
            _dbContext.Clients.Add(client);
            _dbContext.SaveChanges();
        }

        public Client GetById(int id)
        {
            Client client = _dbContext.Clients.Find(id);
            return client;
        }

        public IEnumerable<Client> GetAll()
        {
            IEnumerable<Client> clients = _dbContext.Clients;
            return clients;
        }

        public void Update(Client client)
        {
            var dbPlayer = _dbContext.Clients.Find(client.Id);
            dbPlayer = client;
            _dbContext.SaveChanges();
        }
    }
}
