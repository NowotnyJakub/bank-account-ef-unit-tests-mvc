﻿using System.Data.Entity;
using DataAccess.Entities;

namespace DataAccess
{
    public class BankDbContext : DbContext
    {
        public BankDbContext() : base("BankDbConnectionString")
        {
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<BankDbContext>());
            //Database.SetInitializer(new CreateDatabaseIfNotExists<BankDbContext>());
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<BankDbContext, Migrations.Configuration>());
        }

        public DbSet<Client> Clients { get; set; } 
    }
}
