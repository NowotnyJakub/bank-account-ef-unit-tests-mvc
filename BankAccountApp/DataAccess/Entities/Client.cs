﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
    public class Client
    {
        public int Id { get; set; }
        [Index(IsUnique = true)]
        public float Pesel { get; set; }
        public string Name { get; set; }
        public decimal Money { get; set; }
    }
}
