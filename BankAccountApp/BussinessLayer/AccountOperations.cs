﻿using System.Linq;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;

namespace BussinessLayer
{
    public class AccountOperations
    {
        private readonly IClientRepo _clientRepo;

        public AccountOperations(IClientRepo clientRepo)
        {
            _clientRepo = clientRepo;
        }

        public void AddUserToDb(string userName, float pesel, decimal userBudget)
        {
            Client client = new Client()
            {
                Money = userBudget,
                Pesel = pesel,
                Name = userName,
            };

            _clientRepo.Create(client);
        }

    }
}
