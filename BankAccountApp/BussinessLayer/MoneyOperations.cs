﻿using System;
using System.Linq;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;

namespace BussinessLayer
{
    public class MoneyOperations
    {
        private readonly IClientRepo _clientRepo;

        public MoneyOperations(IClientRepo clientRepo)
        {
            _clientRepo = clientRepo;
        }

        public void AddMoney(Client client, decimal money)
        {
            client.Money += money;
        
            _clientRepo.Update(client);
        }

        public void TransferMoneyToPerson(Client client, decimal moneyToTransfer, float pesel)
        {
            Client targetClient = _clientRepo.GetAll().FirstOrDefault(c => c.Pesel == pesel);
            targetClient.Money += moneyToTransfer;
            client.Money -= moneyToTransfer;

            _clientRepo.Update(client);
            _clientRepo.Update(targetClient);
        }

        public bool IsCorrectPeselToTransfer(float pesel)
        {
            var client = _clientRepo.GetAll().Where(c => c.Pesel == pesel);
            if (client == null)
            {
                return false;
            }
            else
            {
                return false;
            }
        }
    }
}
